const { response, request } = require('express');

const personasGet = (req = request, res = response) => {

    const { q, nombre = 'No name', apikey, page = 1, limit } = req.query;

    res.json({
        msg: 'get API - controlador',
        q,
        nombre,
        apikey,
        page, 
        limit
    });
}

const personasPost = (req, res = response) => {

    const { nombre, edad } = req.body;

    res.json({
        msg: 'post API - personasPost',
        nombre, 
        edad
    });
}

const personasPut = (req, res = response) => {

    const { id } = req.params;

    res.json({
        msg: 'put API - personasPut',
        id
    });
}

const personasPatch = (req, res = response) => {
    res.json({
        msg: 'patch API - personasPatch'
    });
}

const personasDelete = (req, res = response) => {
    res.json({
        msg: 'delete API - personasDelete'
    });
}




module.exports = {
    personasGet,
    personasPost,
    personasPut,
    personasPatch,
    personasDelete,
}