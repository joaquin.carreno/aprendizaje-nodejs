
const { Router } = require('express');

const { personasGet,
    personasPut,
    personasPost,
    personasDelete,
    personasPatch } = require('../controllers/personas');

const router = Router();

router.get('/', personasGet);

router.put('/:id', personasPut);

router.post('/', personasPost);

router.delete('/', personasDelete);

router.patch('/', personasPatch);

module.exports = router;